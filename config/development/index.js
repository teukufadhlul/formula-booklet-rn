export const IGNORE_YELLOW_WARNING = console.disableYellowBox = true;

export const DEFAULT_SCREEN = 'AboutScreen';
export const DEV_MODE = false;
export const LOGGER = false;