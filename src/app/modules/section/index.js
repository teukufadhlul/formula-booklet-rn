import React from 'react';
import { View, ScrollView, Image, FlatList, Text, ActivityIndicator } from 'react-native';

import { compose, withProps, lifecycle, withState } from 'recompose';
import { enhanceSection } from '@domain/container/section'; 
import { Navigation } from 'react-native-navigation';

import { SCREENS } from '@app/navigation/screens';

import { SectionHeader, SectionCard, FilterList } from './component';
import { styles } from './index.style';
import { theme } from '@assets/style';
import { images } from '@assets/img';

const Section = props => {
    return (
        <View style={styles.container} >
            <SectionHeader title={props.title} componentId={props.componentId} />
            <FilterList filterList={props.filter} />
            { props.interaction ? 
                props.data && props.data.length > 0 ?
                    <FlatList
                        data={props.data}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({item, index}) => (
                            <SectionCard data={item} onPressCard={props.onPressCard} />
                        )}
                    />
                    : 
                    <View style={styles.emptyContent}>
                        <Image source={images['undraw_empty']} style={styles.emptyImg} />
                        <Text style={styles.emptyText} >{`Sorry there's nothing here, ${'\n'} we're still workin on some datasheet.`}</Text>
                    </View>
                :
                <ActivityIndicator style={styles.activityIndicator} size="large" color={theme.color.shade} />
            }
        </View>
    )
}

export default enhanceSection(compose(
    withState('interaction', 'setInteraction', false),
    withProps(props => {
        return ({
            onPressCard: (data) => {
                Navigation.push(props.componentId, {
                    component: {
                        name: SCREENS.SUBSECTION,
                        passProps: {
                            data: data.subsection,
                            bgImage: data.image,
                            title: data.title,
                            formFactor: data.formFactor
                        },
                        options: {
                            topBar: {
                                visible: false,
                                drawBehind: true,
                                animate: false
                            },
                            bottomTabs: { 
                                visible: false, 
                                drawBehind: true, 
                                animate: true 
                            } 
                        }
                    }
                });
            },
        })
    }),
    lifecycle({
        componentDidMount() {
            this.navigationEventListener = Navigation.events().bindComponent(this);
            this.props.setInteraction(true)
        },
        componentWillUnmount() {
            // Not mandatory
            if (this.navigationEventListener) {
                this.navigationEventListener.remove();
            }
        },
        componentDidAppear() {
        },
        componentDidDisappear() {
            // Navigation.setComponentId(this.props.componentId)
        }
    })
)(Section));