import React from 'react';
import  Icon  from 'react-native-vector-icons/MaterialIcons';
import { View, TouchableOpacity, Text } from 'react-native';

import { Navigation } from 'react-native-navigation';
import { compose, withHandlers } from 'recompose';

import { headerStyles as styles } from './component.style';

const SectionHeader = props => {
    return(
        <View style={styles.wrapper} >
            <View style={styles.headline} >
                <View>
                    <TouchableOpacity
                        style={styles.homeBtn}
                        onPress={() => props.navigateToRoot()}
                    >
                        <Icon name="home" style={styles.homeBtnIcon}  size={30} color="#FFF" />
                    </TouchableOpacity>
                </View>
                <Text style={styles.headlineTitle} >{props.title}</Text>
                <Text style={styles.headlineSubtitle} >Recomended</Text>
            </View>
        </View>
    )
}

export default compose(
    withHandlers({
        navigateToRoot: props => () => {
            Navigation.popToRoot(props.componentId);
        },
    })
)(SectionHeader)