import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import { Platform, View, Text, TouchableWithoutFeedback, Image } from 'react-native';

import { compose, withHandlers } from 'recompose';


import { theme } from '@assets/style';
import { cardStyles as styles } from './component.style';

const SectionCard = props => {
    return (
        <View style={styles.wrapper} >
            <View style={styles.container} > 
                <View style={styles.content} >
                    <TouchableWithoutFeedback
                        onPress={() => props.handlePress()}
                    >
                        <View style={styles.cardContent} >
                            <Text style={styles.cardTitle} >{props.data.title}</Text>
                            <Text style={styles.cardBody} >{props.data.subtitle}</Text>
                                <Icon name="arrow-right" style={{textAlign: 'right'}} size={30} color={theme.color.shade} />
                        </View>
                    </TouchableWithoutFeedback>
                    <View style={styles.cardBg} >
                        <View style={styles.imgContainer} >
                            <Image style={styles.cardImage} width={'100%'} source={props.data.image} />
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default compose(
    withHandlers({
        handlePress: props => () => {
           props.onPressCard(props.data)
        }
    })
)(SectionCard);