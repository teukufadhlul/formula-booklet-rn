import { StyleSheet } from 'react-native';

import { isIphoneX } from '@domain/phoneLayout'

import { theme } from '@assets/style';

export const headerStyles = StyleSheet.create({
    //================{ Header Style }=============== 
    wrapper: {
        // height: 130,
        width: '100%',
        // backgroundColor: 'grey',
    },
    headline: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    headlineTitle: {
        color: 'black',
        fontSize: 48,
        fontWeight: '600',
        // fontFamily: 'Thasadith',
        marginLeft: 30,
    },
    headlineSubtitle: {
        alignSelf: 'flex-end',
        fontSize: 22,
        fontWeight: '500',
        color: 'rgba(0,0,0,0.6)',
        marginBottom: 7,
        marginLeft: 10
    },
    homeBtn: {
        backgroundColor: theme.color.shade,
        width: 70,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        paddingVertical: 8,
        paddingRight: 8,
    },
    homeBtnIcon: {
        position: 'relative',
        left: '50%',
    },
})

export const cardStyles = StyleSheet.create({
    container: {
        overflow: 'visible',
    },
    content: {
        // flex: 1,
        position: 'relative',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        overflow: 'visible',
        marginHorizontal: 10,
        // backgroundColor: 'red'
    },
    cardContent: {
        top: '10%',
        left: 0,
        position: 'absolute',
        width: '50%',
        backgroundColor: '#FFF',
        shadowOffset: { width: 0, height: 0 }, 
		shadowOpacity: 0.2, 
		shadowRadius: 8,
        elevation: 8,
        zIndex: 10,
        padding: 15,
        borderRadius: 10
    },
    cardBg: {
        width: '65%',
        shadowOffset: { width: 0, height: 0 }, 
		shadowOpacity: 0.2, 
		shadowRadius: 7,
        elevation: 7,
        zIndex: 8,
        borderRadius: 10,
    },
    cardImage: {
        width: 195,
        height: 300
    },
    cardTitle: {
        fontSize: 20,
        fontWeight: "500",
        color: 'black'
    },
    cardBody: {
        fontSize: 12,
        marginTop: 15,
        fontWeight: "normal",
        color: 'black',
        marginBottom: 5,
    },
    imgContainer: {
        overflow: 'hidden',
        backgroundColor: '#FFF',
        borderRadius: 10,
    },
    wrapper: {
        marginTop: 12.5,
        marginBottom: 15,
        paddingHorizontal: '7%',
    },
})

export const filterStyles = StyleSheet.create({
    container: {

    },
    wrapper: {
        // marginTop: 10
        paddingTop: 10,
        paddingBottom: 20
    },
    content: {
        // flexDirection: 'row',
        marginLeft: '9%'
    },
    button: {
        backgroundColor: 'rgba(0, 0, 0, 0.05)',
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 15,
        overflow: 'hidden',
        marginHorizontal: 5
    },
    buttonActive: {
        backgroundColor: theme.color.shade,
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 15,
        overflow: 'hidden',
        marginHorizontal: 5
    },
    textActive: {
        color: 'white',
        fontSize: 17,
    },
    text: {
        fontSize: 17,
        color: 'black',
    }
})