import React from 'react';
import { View, Text, FlatList, TouchableOpacity, ScrollView } from 'react-native';

import { compose, withHandlers, withState } from 'recompose';

import { filterStyles as styles } from './component.style';

const FilterList = props => {
    return (
        <View style={styles.wrapper} >
            <View style={styles.container} >
                <FlatList
                    showsHorizontalScrollIndicator={false}
                    horizontal
                    style={styles.content}
                    data={props.filterList}
                    keyExtractor={(item, index) => index.toString()}
                    extraData={props.activeScreen}
                    renderItem={({item, index}) => (
                        <TouchableOpacity 
                            onPress={() => props.handlePress(index)}
                            style={props.activeScreen == index ? styles.buttonActive : styles.button} >
                            <Text style={props.activeScreen == index ? styles.textActive : styles.text} >{item.filter}</Text>
                        </TouchableOpacity>
                    )}
                />
            </View>
        </View>
    )
}

export default  compose(
    withState('activeScreen', 'setActiveScreen', 0),
    withHandlers({
        handlePress: props => (index) => {
            props.setActiveScreen(index)
            // console.tron(props.activeScreen)
        }
    })
)(FilterList)