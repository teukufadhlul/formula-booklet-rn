import { StyleSheet } from 'react-native';

import { isIphoneX } from '@domain/phoneLayout'

export const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff', 
        height: '100%',
        paddingTop: isIphoneX() ? 35 : 15,
        position: 'relative'
    },
    activityIndicator: { 
        alignSelf: 'center', 
        justifyContent: 'center', 
        height: '75%'
    },
    emptyContent: {
        justifyContent: 'center',
        alignItems: 'center',
        height: '75%'
    },
    emptyImg: {
        width: 250,
        height: 190
    },
    emptyText: {
        textAlign: 'center',
        color: 'black'
    }
})