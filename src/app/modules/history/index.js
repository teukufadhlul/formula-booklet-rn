import React from 'react';
import { View, Text, FlatList, ScrollView, Image } from 'react-native';

import { compose, lifecycle, withState, withProps } from 'recompose';
import { enhanceHistory } from '@domain/container/history'; 
import { Navigation } from 'react-native-navigation';
import { removeHistory } from '@app/redux/action';
import { connect } from 'react-redux';

import { SCREENS } from '@app/navigation/screens';

import { FilterList, HistoryCard } from './component'
import { styles } from './index.style';
import { images } from '@assets/img';

const History = props => {

    return (
        <ScrollView scrollEnabled={props.scrollEnabled} style={styles.wrapper} >
            <View style={styles.container} >
                <FilterList onPressFilter={props.onPressFilter} timeFilter={props.timeFilter} />
                { props.historyData && props.historyData.length > 0 ? 
                    <FlatList
                        scrollEnabled={false}
                        data={props.historyData}
                        keyExtractor={(item, index) => index.toString()}
                        extraData={props.refresh}
                        renderItem={({item, index}) => (
                            <HistoryCard 
                                data={item} 
                                setScrollEnabled={props.setScrollEnabled}
                                onPressRemove={() => props.onPressRemove(index)}
                                componentId={props.componentId}
                                onPress={() => props.onPressCard(item)} />
                        )}
                    />
                    :
                    <View style={styles.emptyContent}>
                        <Image source={images['undraw_empty']} style={styles.emptyImg} />
                        <Text style={styles.emptyText} >{`It's empty box, ${'\n'} seek some formula to fill the history`}</Text>
                    </View>
                }
            </View>
        </ScrollView>
    );
};

const mapStateToProps = state => ({
    subsectionHistory: state.subsectionHistory.history
})

export default enhanceHistory(compose(
    connect(mapStateToProps),
    withState('refresh', 'setRefresh', false),
    withState('timeFilter', 'setTimeFilter', 'all'),
    withState('historyData', 'setHistoryData', []),
    withState('scrollEnabled', 'setScrollEnabled', true),
    withProps(props => {
        return ({
            onPressCard: (data) => {
                Navigation.push(props.componentId, {
                    component: {
                        name: SCREENS.COURSE,
                        passProps: {
                            data: data,
                        },
                        options: {
                            topBar: {
                                visible: false,
                                drawBehind: true,
                                animate: false
                            },
                            bottomTabs: { 
                                visible: false, 
                                drawBehind: true, 
                                animate: true 
                            } 
                        }
                    }
                });
            },
            onPressFilter: (timeFilter) => {
                props.setTimeFilter(timeFilter)

                const newData = props.subsectionHistory.filter(e => {
                    let date = new Date(e.createdAt)
                    let today = new Date()
                    let yesterday = new Date()
                    
                    yesterday.setDate(yesterday.getDate() -1)

                    switch (timeFilter) {
                        case 'today':
                            if (date.toDateString() == today.toDateString()) {
                                return e
                            }
                            break;
                        case 'yesterday':
                            if (date.toDateString() == yesterday.toDateString()) {
                                return e
                            }
                            break;
                        default:
                            return e
                    }

                })

                props.setHistoryData(newData)
                props.setRefresh(!props.refresh)

            },
            onPressRemove: index => {
                props.dispatch(removeHistory(props.subsectionHistory, index))
                props.setHistoryData(props.subsectionHistory)
                props.setRefresh(!props.refresh)
            }
        })
    }),
    lifecycle({
        componentDidMount(){
            this.props.setHistoryData(this.props.subsectionHistory)
            this.navigationEventListener = Navigation.events().bindComponent(this);
        },
        componentDidAppear() {
            this.props.setHistoryData(this.props.subsectionHistory)
            this.props.setRefresh(!this.props.refresh)
        }
    })
)(History))