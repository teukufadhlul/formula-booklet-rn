import { StyleSheet } from 'react-native';
import { isIphoneX } from '@domain/phoneLayout'

export const styles = StyleSheet.create({
    wrapper: {
        flex: 1
    },
    container: {
        marginTop: isIphoneX() ? 0 : 10,
    },
    text : {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    },
    emptyContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '40%'
    },
    emptyImg: {
        width: 250,
        height: 190
    },
    emptyText: {
        textAlign: 'center',
        color: 'black'
    }
})