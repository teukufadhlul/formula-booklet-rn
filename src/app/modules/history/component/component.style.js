import { StyleSheet } from 'react-native';

import { isIphoneX } from '@domain/phoneLayout'

import { theme } from '@assets/style';

export const filterStyles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    wrapper: {
        // marginTop: 10
        paddingTop: 10,
        paddingBottom: 20
    },
    content: {
        // flexDirection: 'row',
        marginLeft: '9%'
    },
    button: {
        backgroundColor: 'rgba(0, 0, 0, 0.05)',
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 15,
        overflow: 'hidden',
        marginHorizontal: 5
    },
    buttonActive: {
        backgroundColor: theme.color.shade,
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 15,
        overflow: 'hidden',
        marginHorizontal: 5
    },
    textActive: {
        color: 'white',
        fontSize: 14,
    },
    text: {
        fontSize: 14,
        color: 'black',
    }
})

export const cardStyles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: '#FFFF',
        overflow: 'visible',
        // elevation: 4,
        // zIndex: 8,
        borderRadius: 10,
        // shadowColor: "#000", 
		// shadowOffset: { width: 0, height: 0 }, 
		// shadowOpacity: 0.2, 
		// shadowRadius: 8,
    },
    content: {
        flexDirection: 'row',
        width: '100%',
        padding: 12,
        
    },
    formula: {
        marginTop: 5,
        fontSize: 12,
        color: 'black',
        fontWeight: '400'
    },
    imageWrapper: {
        width: '45%'
    },
    image: {
        width: 147,
        height: 121
    },
    readMore: {
        marginTop: 10,
        fontSize: 14,
        color: theme.color.shade
    },
    subtitle: { 
        fontSize: 11,
        color: 'black',
        marginTop: 5,
    },
    swipeContainer: {
        elevation: 4,
        zIndex: 8,
        shadowColor: "#000", 
		shadowOffset: { width: 0, height: 0 }, 
		shadowOpacity: 0.2, 
        shadowRadius: 8,
        overflow: 'visible',
        borderRadius: 10,
    },
    swipeOut: {
        backgroundColor: 'white',
        borderRadius: 10,
    },
    titleWrapper: {
        width: '55%',
        marginRight: 5,
    },
    title: {
        color: 'black',
        fontSize: 20,
        fontWeight: '500',
    },
    wrapper: {
        flex: 1,
        paddingHorizontal: 20,
        marginVertical: 10,
    },
})