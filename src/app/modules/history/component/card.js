import React from 'react';
import Swipeout from 'react-native-swipeout';
import { View, TouchableWithoutFeedback, Text, Image, Animated  } from 'react-native';

import { compose, withHandlers, withProps, lifecycle, onlyUpdateForKeys, withState } from 'recompose';

import { cardStyles as styles } from './component.style';

const swipeoutBtns = [
    { 
        text: 'Delete',
        type: 'delete',
    }
]

const HistoryCard = props => {
    return (
        <View style={styles.wrapper} >
            <Animated.View style={[styles.swipeContainer, {opacity: props._animated}]}>
                <Swipeout 
                    autoClose={true}
                    close={props.closeSwipeout}
                    onOpen={(sectionID, rowId, direction) => {
                        props.setScrollEnabled(false)
                        if (sectionID == -1 && rowId == -1 && direction == "right"){
                            props.setCloseSwipeout(true)
                            props.setScrollEnabled(false)
                            setTimeout(() => {
                                Animated.timing(props._animated, {
                                    toValue: 0,
                                    duration: 250,
                                }).start(() => {
                                    props.onPressRemove()
                                });
                            }, 50)
                        }
                    }}
                    onClose={(sectionID, rowId, direction) => {
                        props.setScrollEnabled(true)
                    }}
                    right={swipeoutBtns} 
                    style={styles.swipeOut}>
                    <TouchableWithoutFeedback onPress={props.onPress} >
                        <View style={styles.container} >
                            <View style={styles.content} >
                                <View style={styles.titleWrapper} >
                                    <Text style={styles.title} >{props.data.title}</Text>
                                    <Text style={styles.formula} >{}</Text>
                                    <Text style={styles.subtitle } >{props.data.subtitle}</Text>
                                    <Text style={styles.readMore} >Lebih Lanjut</Text>
                                </View>    
                                <View style={styles.imageWrapper} >
                                    <Image style={styles.image} source={props.data.image} />
                                </View>    
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Swipeout>
            </Animated.View>
        </View>
    )
}

export default compose(
    withProps( props => ({
        _animated: new Animated.Value(1),
    })),
    withState('closeSwipeout', 'setCloseSwipeout', false),
    lifecycle({
        componentDidUpdate(){
            if (this.props.closeSwipeout){
                this.props.setCloseSwipeout(false)
            }
            Animated.timing(this.props._animated, {
                toValue: 1,
                duration: 1,
            }).start();
        }
    }),
)(HistoryCard);