import React from 'react';
import { View, Text, FlatList, TouchableOpacity, ScrollView } from 'react-native';

import { compose, withHandlers, withState } from 'recompose';

import { filterStyles as styles } from './component.style';

const FilterList = props => {
    return (
        <View style={styles.wrapper} >
            <View style={styles.container} >
                <TouchableOpacity 
                    onPress={() => props.handlePress('today')}
                    style={props.timeFilter == 'today' ? styles.buttonActive : styles.button} >
                    <Text style={props.timeFilter == 'today' ? styles.textActive : styles.text} >Today</Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    onPress={() => props.handlePress('all')}
                    style={props.timeFilter == 'all' ? styles.buttonActive : styles.button} >
                    <Text style={props.timeFilter == 'all' ? styles.textActive : styles.text} >All</Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    onPress={() => props.handlePress('yesterday')}
                    style={props.timeFilter == 'yesterday' ? styles.buttonActive : styles.button} >
                    <Text style={props.timeFilter == 'yesterday' ? styles.textActive : styles.text} >Yesterday</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default  compose(
    withHandlers({
        handlePress: props => (index) => {
            props.onPressFilter(index)
        }
    })
)(FilterList)