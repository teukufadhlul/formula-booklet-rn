import React from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView, Platform } from 'react-native';

import AppLink from 'react-native-app-link';
import { compose, withHandlers } from 'recompose';
import { enhanceAbout } from '@domain/container/about'

import { styles } from './index.style';

import Shimmer from 'react-native-shimmer';

const About = props => {
    return (
        <ScrollView style={styles.wrapper} >
            <View
                style={[styles.container, {flex: 1}]}>
                <Image 
                    style={styles.image}
                    source={require('@assets/img/lonely-thinker.png')}
                />
                <View>
                    <Text style={styles.title} >The Ascetic Writer</Text>
                    <Text style={styles.article} >
                        Hi there, thanks for downloading this app.
                        {"\n"}{"\n"}
                        This application is made to help students learn and shorten the wasted time. Hope you enjoy learning with this application. Happy Learning!
                        {"\n"}{"\n"}
                        I am a 20 years old guy who passionate about computer science, music and art. currently living in Jakarta.
                    </Text>
                    <View style={styles.socialMedia} >
                        <TouchableOpacity
                            onPress={() => {
                                props.handleApplinkPress("instagram://user?username=teukufadh", { appName: "instagram", appStoreId: '389801252', appStoreLocale: 'us', playStoreId: 'com.instagram.android' })
                            }}> 
                            <Image
                                source={require('@assets/img/instagram.png')}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                props.handleApplinkPress("twitter://user?screen_name=teukufadh", { appName: "twitter", appStoreId: '333903271', appStoreLocale: 'jo', playStoreId: 'com.twitter.android' })
                            }}> 
                            <Image
                                source={require('@assets/img/twitter.png')}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                props.handleApplinkPress("https://www.linkedin.com/in/teukufadhlul/", {})
                            }}> 
                            <Image
                                source={require('@assets/img/linkedin.png')}
                            />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.version} >Alpha v0.01</Text>
                </View>
                <View style={{marginTop: 70, paddingBottom: 20}} >
                    <Text style={styles.title} >DMCA</Text>
                    <Text style={styles.article} >
                        We respect the copyright laws and will protect the right of every copyright owner seriously. 
                        {"\n"}{"\n"}
                        If you are the owner of any content showed on this apps and you dont want to allow us to use the content, then you are very able to tell us by email to formulabooklet@gmail.com so that we can identify and take necessary action.
                        {"\n"}{"\n"}
                        We cannot take any action if you dont give us any information about it, so please send us an email with the details.
                    </Text> 
                </View>
            </View>
        </ScrollView>
    );
};

export default enhanceAbout(compose(
    withHandlers({
        handleApplinkPress: props => (url, config) => {
            AppLink.maybeOpenURL(url, { ...config })
                .then(res => {

                }).catch(err => {

                })
        }
    })
)(About));