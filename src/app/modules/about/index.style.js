import { StyleSheet } from 'react-native';

import { isIphoneX } from '@domain/phoneLayout'

export const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: '#FFFF',
    },
    container: {
        marginTop: isIphoneX() ? 35 : 0,
        paddingHorizontal: 50,
        flexDirection: 'column',
        paddingTop: isIphoneX() ? 80 : 30  
    },
    image: {
        marginTop: 50,
        alignSelf: 'center',
        width: 207.48,
        height: 172.56
    },
    article: {
        textAlign: 'center',
        color: 'black',
        fontSize: 12,
        marginTop: 30
    },
    title: {
        marginTop: 30,
        textAlign: 'center',
        color: 'black',
        fontSize: 25,
        fontWeight: '600',
    },
    socialMedia: {
        marginTop: 50,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        paddingHorizontal: 80
    },
    version: {
        marginTop: 15,
        color: 'grey',
        fontSize: 8,
        alignSelf: 'center'
    }
})