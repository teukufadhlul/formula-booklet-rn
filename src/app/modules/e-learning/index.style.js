import { StyleSheet } from 'react-native';

import { theme } from '@assets/style'

import { isIphoneX } from '@domain/phoneLayout';

export const styles = StyleSheet.create({
    buttonText: {
        color: 'white',
        fontWeight: '400',
        fontSize: 15,
        textAlign: 'center'
    },  
    container: {
        paddingHorizontal: 50,
        // marginBottom: 50,
    },
    article: {
        color: 'black',
        fontSize: 13,
        marginTop: 20
    },  
    image: { 
        alignSelf: 'center',
        width: 233.41,
        height: 177.75,
        transform: [
            {rotateY: '180deg'}
        ]
    },
    text : {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    },
    title: {
        color: 'black',
        fontWeight: '600',
        fontSize: 25,
        marginTop: 25
    },
    wrapper: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        backgroundColor: '#FFFF'
    },
    button: {
        backgroundColor: theme.color.secondary,
        borderRadius: 10 ,
        width: 110,
        paddingHorizontal: 15,
        paddingVertical: 7,
        marginTop: 50
    }
})