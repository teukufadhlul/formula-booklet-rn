import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

import { compose, withProps, withHandlers } from 'recompose'
import { enhanceELearning } from '@domain/container/e-learning';
import { Navigation } from 'react-native-navigation';

import { SCREENS } from '@app/navigation/screens';

import { styles } from './index.style';

import { deleteAllLessons } from '@data/realm/queries/lesson'

const ELearning = props => {
    return (
        <View style={styles.wrapper} >
            <View style={styles.container} >
                <Image
                    style={styles.image} 
                    source={require('@assets/img/undraw_fishing.png')} />
                <View>
                    <Text style={styles.title} >It'll be ready {"\n"}when it's ready...</Text>
                    <Text style={styles.article} >
                        Since I worked on this application alone without a team, it took a lot of time to develop this feature. please wait until this feature is complete, or if you have the ability and want to form a team, please don't hesitate to contact me
                    </Text>
                    <TouchableOpacity onPress={() => props.handleSubmit()} style={styles.button}>
                        <Text style={styles.buttonText} >Contact me</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

export default enhanceELearning(compose(
    withProps(props => {
        return ({
            onPressBtn: () => {
                Navigation.push(props.componentId, {
                    component: {
                        name: SCREENS.ABOUT,
                        options: {
                            topBar: {
                                visible: false,
                                drawBehind: true,
                                animate: false
                            },
                            bottomTabs: { 
                                visible: false, 
                                drawBehind: true, 
                                animate: true 
                            } 
                        }
                    }
                });
            },
        })
    }),
    withHandlers({
        handlePressBtn: props => () => {
            props.onPressBtn()
        },
        handleSubmit: props => () => {
            deleteAllLessons('Lesson').then(res => alert('berhasil menghapus data')).catch(err => {
                // console.tron(err)
            })
        }
    })
)(ELearning));