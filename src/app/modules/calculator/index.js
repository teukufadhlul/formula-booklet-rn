import React from 'react';
import { View, Text } from 'react-native';

import { enhanceCalculator } from '@domain/container/calculator'

import { styles } from './index.style';


const Calculator = props => {
    return (
        <View style={styles.wrapper} >
            <Text style={styles.text} >
                Calculator Screen
            </Text>
        </View>
    );
};

export default enhanceCalculator(Calculator);