import { StyleSheet } from 'react-native';

import { theme } from '@assets/style'
import { isIphoneX, isAndroid } from '@domain/phoneLayout'

export const styles = StyleSheet.create({
   card: {
      alignSelf: 'center',
      marginVertical: 15,
      paddingHorizontal: 20,
      paddingVertical: 20,
      borderColor: '#FFF',
      borderRadius: 10,
      shadowColor: "#000", 
      shadowOffset: { width: 0, height: 0 }, 
      // overflow: 'hidden',
      shadowOpacity: 0.2, 
      shadowRadius: 8,
      elevation: 6,
      zIndex: 10,
      position: "relative",
      backgroundColor: 'white',
      width: '80%',
   },
   cardFormula: {
      paddingVertical: isAndroid() ? 0 : 20, 
      flexDirection: 'row',
      alignItems: 'center', 
      // justifyContent: 'center'
   },
   container: {
      top: isIphoneX() ? 35 : 10,
   },
   content: {
      alignItems: 'center',
      marginBottom: 100,
      flex: 1
   },
   formula: {
      textAlign: 'center',
      color: 'black'
   },
   fxButton: {
      borderRadius: 10, 
      backgroundColor: theme.color.secondary, 
      position: 'absolute', 
      right: 0, 
      alignItems: 'center',
      alignSelf: 'center',
      justifyContent: 'center',
      paddingHorizontal: 10,
      height: isAndroid() ? '100%' : '330%',
      zIndex: 10, 
      // elevation: 20 
   },
   headline: {
      fontWeight: 'bold',
      fontSize: 20,
      color: 'black'
   },
   image: {
      height: 215,
      width: 250,
      alignSelf: 'center',
   },
   paragraph: {
      color: 'black',
      fontSize: 13
   },
   wrapper: {
      height: '100%',
      backgroundColor: 'white'
   },
   texInput: {
      textAlign: 'center'
   },
   textVariable: {
      paddingHorizontal: !isAndroid() ? 5 : 0, 
      margin: 0, 
      color: 'black'
   },
   toast: {
      backgroundColor: theme.color.danger
   }
})