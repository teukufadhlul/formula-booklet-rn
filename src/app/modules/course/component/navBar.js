import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import { View, Text, TouchableOpacity, Vibration } from 'react-native';

import { compose, withHandlers, withProps } from 'recompose';
import { Navigation } from 'react-native-navigation';


import { navbarStyles as styles } from './component.style'

const Navbar = props => {
    return(
        <View style={styles.navbar} >
            <TouchableOpacity
                onPress={props.handleBack}
                onLongPress={props.handleLongPressBack}>
                <Icon name="arrow-left" style={styles.back} size={30} />
            </TouchableOpacity>
            <Text style={styles.title} >{props.title}</Text>
            <TouchableOpacity
                // onPress={props.handleCalculate}
            >
                <Icon name="bar-chart" style={styles.menu} size={30} />
            </TouchableOpacity>
        </View>
    )
}

export default compose(
    withHandlers({
        handleBack: props => () => {
            Navigation.pop(props.componentId);
        },
        handleLongPressBack: props => () => {
            Vibration.vibrate(100)
            Navigation.popToRoot(props.componentId)
        }
    })
)(Navbar)