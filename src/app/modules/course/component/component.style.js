import { StyleSheet, Platform } from 'react-native';

import { theme } from '@assets/style';
import { isIphoneX } from '@domain/phoneLayout'

export const navbarStyles = StyleSheet.create({
    back: {
        textAlign: 'left', 
        color: theme.color.shade
    },
    menu: {
        textAlign: 'right', 
        color: theme.color.shade,
        transform: [{ rotate: '-90deg'}]
    },
    navbar: {
        height: 50,
        paddingHorizontal: 20,
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    title: {
        color: theme.color.shade,
        fontSize: 18,
        textAlign: 'center',
        fontWeight: '600',
        alignSelf: 'center',
    },
})