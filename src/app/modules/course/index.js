import React from 'react';
import Toast, {DURATION} from 'react-native-easy-toast'
import RBSheet from "react-native-raw-bottom-sheet";
import { View, Text, Image, ScrollView, FlatList, TextInput, TouchableOpacity, Button, TouchableWithoutFeedback } from 'react-native';

import { compose, withHandlers, withState, withProps, lifecycle } from 'recompose';
import { enhanceCourse } from '@domain/container/course';
import { Navigation } from 'react-native-navigation';

import { Navbar } from './component';
import { styles } from './index.style';
import { images } from '@assets/img';

import { theme } from '@assets/style';

const Course = props => {
   
    return (
        <View style={styles.wrapper} >
            <View style={styles.container} >

                <Navbar handleCalculate={props.handleCalculate} title={props.data.title} componentId={props.componentId} />

                <Toast 
                    style={styles.toast}
                    position='top'
                    positionValue={200}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    ref={(ref) => props.setToastRef(ref)}/>

                <ScrollView>
                    <Image
                        source={images[`${props.data.inventorImg}`]}
                        style={styles.image} />

                    <View style={styles.content}>
                        {
                            <FlatList
                                scrollEnabled={false}
                                style={{width: '100%', paddingBottom: 20}}
                                data={props.data.formula}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({item, index}) => (
                                    <View key={index} style={[styles.card, styles.cardFormula]} >
                                                {item.formula.split(" ").map((item, _index) => {
                                                    if (item.match(/[a-zA-Z]/g)){
                                                        return (
                                                            <TextInput
                                                                onFocus={() => props.setIndex(index)}
                                                                onChangeText={(values) => props.setVariables({...props.variables, [item]: values === "" ? null : values})}
                                                                key={_index}
                                                                value={props.index == index ? props.variables[item] || "" : ""}
                                                                keyboardType='numeric'
                                                                style={styles.texInput}
                                                                placeholder={item}
                                                                underlineColorAndroid='rgba(0,0,0,0)'
                                                                autoCorrect={false}
                                                            />
                                                        )
                                                    } else if (item == '*') {
                                                        return <Text key={_index} >•</Text>
                                                    } else {
                                                        return <Text key={_index} style={styles.textVariable}> {item} </Text>
                                                    }
                                                })}
                                        <TouchableOpacity
                                            onPress={() => {
                                                props.setIndex(index)
                                                props.handleCalculate(item.formula)
                                            }}
                                            style={styles.fxButton} >
                                                <Text style={{color: 'white'}}> {index == props.index ? props.result ? props.result.result.reverse()[0] : "ƒx" : "ƒx"} </Text>
                                        </TouchableOpacity>
                                    </View>
                                )}
                            />
                        }

                        <View style={styles.card} >
                            <Text style={styles.headline} >Keterangan</Text>
                            <Text style={styles.paragraph} >{props.data.subtitle}</Text>
                        </View>
                        
                        <View style={styles.card} >
                            <Text style={styles.headline} >Materi</Text>
                            <Text style={styles.paragraph} >{props.data.explanation}</Text>
                        </View>

                        <RBSheet
                            ref={ref => {
                                props.setSheetRef(ref);
                            }}
                            closeOnSwipeDown
                            height={250}
                            duration={250}
                            customStyles={{
                                container: {
                                    justifyContent: "center",
                                    alignItems: "center",
                                    borderTopStartRadius: 30,
                                    borderTopEndRadius: 30
                                }
                            }}
                        >   
                            { props.result && 
                                props.result.result.map((item, index) => (
                                    <Text style={{paddingVertical: 10, fontSize: 15}} key={index} >{item}</Text>
                                ))
                            }
                        </RBSheet>

                    </View>
                    
                </ScrollView>
            </View>
        </View>
    )
}

export default enhanceCourse(compose(
    withState('variables', 'setVariables', {}),
    withState('index', 'setIndex', null),
    withState('result', 'setResult', null),
    withState('textInput', 'setTextInput', null),
    withHandlers(() => {
        let toastRef;
        let sheetRef;
        return {
            setToastRef: props => _ref => {
                toastRef = _ref
            },
            setSheetRef: props => _ref => {
                sheetRef = _ref
            },
            handleCalculate: props => _formula => {
               
                let equation;
                
                try {
                    equation = props.calculate(_formula, props.variables)
                    props.setResult(equation)
                    sheetRef.open()
                } catch(err) {
                    toastRef.show('Please check your variables.');
                }
            }
        }
    }),
    lifecycle({
        componentDidMount() {
            this.navigationEventListener = Navigation.events().bindComponent(this);
        },
        componentDidUpdate(prevProps){
            if (this.props.index) {
                if (prevProps.index !== this.props.index) {
                    this.props.setResult(null)
                    this.props.setVariables({})
                }
            }
            if (this.props.variables) {
                if (prevProps.variables !== this.props.variables) {
                    this.props.setResult(null)
                }
            }
        },
        componentDidDisappear() {
            // Navigation.setComponentId(this.props.componentId)
        }
    })
)(Course));