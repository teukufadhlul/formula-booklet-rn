import { StyleSheet, Platform } from 'react-native';

import { theme } from '@assets/style';
import { isIphoneX } from '@domain/phoneLayout'

export const styles = StyleSheet.create({
    content: {
        // flex: 1,
        height: '95%',
        // alignItems: 'center',
        justifyContent: 'center',
        overflow: 'visible',
        paddingHorizontal: 10
    },
    cardWrapper: {
        // marginTop: Platform.OS === 'ios' ? 20 : 5,
        backgroundColor: '#FFF',
        overflow: 'visible',
        height: isIphoneX() ? '80%' : '88%',
        position: 'relative',
        top: '-5%',
        borderColor: '#FFF',
        borderRadius: 10,
        shadowColor: "#000", 
		shadowOffset: { width: 0, height: 0 }, 
		shadowOpacity: 0.2, 
		shadowRadius: 8,
        elevation: 8,
        zIndex: 10,
    },
    card: {
        overflow: 'hidden',
        height: '100%',
        width: '100%',
        paddingHorizontal: 40,
        paddingBottom: 40,
    },
    imgCard: {
        width: 156,
        height: 130,
        position: 'absolute',
        transform: [{ rotate: '-90deg'}],
        top: 30,
        right: -15
    },
    cardHeader: {
        marginTop: 110,
    },
    cardTitle : {
        fontSize: 25,
        color: 'black',
        // fontFamily: theme.fonts.primary,
        fontWeight: 'bold',
    },
    cardLecture: {
        marginTop: 10,
        fontSize: 10,
        color: 'black'
    },
    cardAuthor: {
        fontSize: 8,
        color: 'rgba(0,0,0,0.7)'
    },
    cardBody: {
        marginTop: 20
    },
    cardBodyText: {
        fontSize: 12,
        color: 'black'
    }
})