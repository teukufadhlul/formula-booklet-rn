import React from 'react';
import { Image, View, TouchableWithoutFeedback, Text } from 'react-native';

import { compose, withHandlers, withState, withProps } from 'recompose';

import { styles } from './component.style';

const CardLesson = props => {
    return (
        <TouchableWithoutFeedback  
            disabled={props.pressed}
            onPress={() => props.handlePress()} >
            <View
                style={styles.content} >
                <View style={styles.cardWrapper} >
                    <View style={styles.card} >
                        <Image
                            source={props.data.image}
                            style={styles.imgCard}
                        />
                        <View style={styles.cardHeader} >
                            <Text style={styles.cardTitle} >{props.data.title}</Text>
                            <Text style={styles.cardLecture} >{props.data.lectures} Lectures</Text>
                            <Text style={styles.cardAuthor} >By {props.data.author}</Text>
                        </View>
                        <View style={styles.cardBody} >
                            <Text style={styles.cardBodyText} >
                                {props.data.description}
                            </Text>
                        </View>
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback>
    )
}

export default compose(
    withState('pressed', 'setPressed', false),
    withHandlers({
        handlePress: props => () => {
           props.onPressCard(props.data)
        }
    })
)(CardLesson)

    
