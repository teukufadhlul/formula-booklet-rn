import { StyleSheet } from 'react-native';

import { isIphoneX, isScreen16, height, width } from '@domain/phoneLayout'

export const styles = StyleSheet.create({
    wrapper: {
        paddingTop: isIphoneX() ? 0 : 0
    },
    activityIndicator: { 
        alignSelf: 'center', 
        justifyContent: 'center', 
        height: '100%'
    },
    contentWrapper: {
        
    },
    text : {
        // fontSize: 20,
        // textAlign: 'center',
        // margin: 10,
        // backgroundColor: 'red'
    },
    imageBgBottom: {
        width: 518,
        height: 431,
        position: 'absolute',
        transform: [{ rotate: '42deg'}],
        bottom: -40,
        left: -130
    },
    imageBgTop: {
        width: 316,
        height: 263,
        position: 'absolute',
        transform: [{ rotate: '202deg'}],
        top: -170,
        right: -70
    },
    swiper: {
        overflow: 'visible'
    },
    pagination: {
        top: '-80%',
    }
})