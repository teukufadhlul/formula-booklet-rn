import React from 'react';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { View, Image, Dimensions, ActivityIndicator  } from 'react-native';

import { compose, withState, lifecycle, withProps, withHandlers, onlyUpdateForKeys } from 'recompose'
import { enhanceLessons } from '@domain/container/lessons'; 
import { Navigation } from 'react-native-navigation';
import { isIphoneX } from '@domain/phoneLayout'

import { connect } from 'react-redux'

import { getAllLessons, writeLesson } from '@data/realm/queries';
import { initialData } from '@data/initial-data';
import { SCREENS } from '@app/navigation/screens'

import { styles } from './index.style';
import { theme } from '@assets/style';
import { images } from '@assets/img';

import { AdsWrapper } from '@app/components';
import { CardLesson } from './component';

const Lessons = props => {
    const data = []

    props.lesson.map((item, index) => {
        let section = []

        if (item.section !== 0 ){
            item.section.map((item) => {
                let subsection = []
                if (item.subsection  !== 0 ){
                    item.subsection.map((item) => {
                        let formula = []
                        if (item.formula !== 0) {
                            item.formula.map((item) => {
                                formula.push({
                                    ...item
                                })
                            })
                        }
                        subsection.push({
                            ...item,
                            image: images[item.image],
                            formula
                        })
                    })
                }
                section.push({
                    ...item,
                    image: images[item.image],
                    subsection
                })
            })
        }
        data.push({
            ...item, 
            title: item.subjects,
            lectures: item.section.length,
            image: images[item.image],
            section
        })
    })

    const deviceWidth = Dimensions.get('window').width

    const handleScroll = (index) => {
        props.setActiveDotIndex(index)
    }

    return (
        <View>
            { !props.interaction ?
                <ActivityIndicator style={styles.activityIndicator} size="large" color={theme.color.shade} />
            :
                <View style={styles.wrapper} >
                    <AdsWrapper
                        fallbackAds={[
                            "https://miro.medium.com/max/720/1*ibN0ptxrg6LkZ5DZbKv2YA.png"
                        ]}
                        googleMobAds={{
                            ios: 'ca-app-pub-1393182479030786/3570593746',
                            android: 'ca-app-pub-1393182479030786/3083562487',
                        }}
                        onWatched={props.onRewarded}
                        onAdClosed={props.onAdClosed} >
                        {(showAds) => {
                            props.assignShowAdsHandler(showAds);
                        }}
                    </AdsWrapper>
                    <Image
                        source={data[props.activeDotIndex].image}
                        style={[styles.imageBgBottom, props.activeDotIndex == 1 && {bottom: -80, left: -160}]} 
                    />
                    <Image
                        source={data[props.activeDotIndex].image}
                        style={styles.imageBgTop}
                    />
                    <Pagination
                        dotsLength={data.length}
                        activeDotIndex={props.activeDotIndex}
                        dotStyle={{
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            marginHorizontal: 1,
                            marginTop: isIphoneX() ? 20 : 0,
                            backgroundColor: theme.color.shade
                        }}
                        inactiveDotStyle={{
                            // Define styles for inactive dots here
                        }}
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                    />
                    <Carousel
                        data={data}
                        renderItem={({item, index}) => <CardLesson onPressCard={props.onPressCard} data={item} />}
                        windowSize={1}
                        style={{oveflow: 'visible'}}
                        sliderWidth={deviceWidth}
                        itemWidth={deviceWidth * 80/100}
                        layout={'default'}
                        slideStyle={{overflow: 'visible'}}
                        onBeforeSnapToItem={(index) => handleScroll(index) }
                        lockScrollWhileSnapping
                        inactiveSlideOpacity={1}
                    />
                </View>
            }
        </View>
    );
};

export default enhanceLessons(compose(
    connect(),
    withState('showAds', 'setShowAds', () => null),
    withState('lesson', 'setLessons', []),
    withState('activeDotIndex', 'setActiveDotIndex', 0),
    withState('interaction', 'setInteraction', false),
    withProps(props => {
        return ({
            onPressCard: (data) => {
                Navigation.push(props.componentId, {
                    component: {
                        name: SCREENS.SECTION,
                        passProps: {
                            data: data.section,
                            filter: data.filter,
                            title: data.title
                        },
                        options: {
                            topBar: {
                                visible: false,
                                drawBehind: true,
                                animate: false
                            },
                            bottomTabs: { 
                                visible: false, 
                                drawBehind: true, 
                                animate: true 
                            } 
                        }
                    }
                });
            },
            onRewarded: props => () => {
                // console.tron('Rewarded');
            },
            onAdClosed: props => () => {
                // console.tron('Ads Closed');
            },
        })
    }),
    withHandlers(() => {
        let showAds;
        return {
            assignShowAdsHandler: props => params => {
                showAds = params;
            },
            showAds: props => () => {
                showAds()
            }
        }
    }),
    lifecycle({
       
        async componentDidMount(){
            
            await getAllLessons().then( res => {
                this.props.setLessons(res)
            }).catch( err => {
                this.props.setLessons([])
            })
            
            if (this.props.lesson.length == 0) {
                await writeLesson(initialData).then(res => {
                    // console.tron('realm initial data respons =>', res)
                }).catch(err => {
                    alert(err)
                })
            }
            
            this.props.setInteraction(true)
            
        }
    }),
)(Lessons))
