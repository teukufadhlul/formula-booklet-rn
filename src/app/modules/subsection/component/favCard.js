import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import { favCardStyles as styles } from './component.style';

const FavCard = props => {
    return (
        <View style={styles.container} >
            <View style={styles.content} >
                {/* <Icon style={styles.favButton} name="heart" /> */}
                <Text numberOfLines={2} style={styles.title} >{props.data.title}</Text>
                {props.data.formula &&
                    <Text style={styles.formula} >{props.data.formula[0].formula}</Text>
                }
                <View style={styles.navButton} >
                    <Text style={styles.navButtonTitle} >Hitung</Text>
                    <Icon style={styles.navButtonIcon} name="chevron-right" />
                </View>
            </View>
        </View>
    )
}

export default FavCard;