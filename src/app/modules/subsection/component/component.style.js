import { StyleSheet, Platform } from 'react-native';
import { theme } from '@assets/style';

import { isIphoneX } from '@domain/phoneLayout';

export const navbarStyles = StyleSheet.create({
    navBar: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        alignContent: 'center',
        zIndex: 5,
        elevation: 5,
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    back: {
        textAlign: 'left', 
        color: 'white'
    },
    search: {
        textAlign: 'right', 
        color: 'white',
    },
    searchInputWrapper: {
        alignContent: 'center',
        width: '70%',
        borderRadius: 10,
        overflow: 'hidden',
        backgroundColor: 'rgba(255,255,255,0.2)'
    },
    searchInput: {
        color: 'white',
        width: '100%',
        height: 30,
        padding: 0,
        fontSize: 14,
        alignSelf: 'center',
        textAlign: 'center',
    },
    title: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        fontWeight: '600',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
        alignSelf: 'center',
    },
    
})

export const listChaptersCardStyles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: '#FFFF',
        overflow: 'visible',
        borderRadius: 10,
    },
    content: {
        flexDirection: 'row',
        width: '100%',
        padding: 12
    },
    formula: {
        marginTop: 5,
        fontSize: 12,
        color: 'black',
        fontWeight: '400'
    },
    imageWrapper: {
        width: '45%'
    },
    image: {
        width: 147,
        height: 121
    },
    readMore: {
        marginTop: 10,
        fontSize: 14,
        color: theme.color.shade
    },
    subtitle: { 
        fontSize: 12,
        color: 'black',
        marginTop: 5,
    },
    swipeContainer: {
        elevation: 4,
        zIndex: 8,
        shadowColor: "#000", 
		shadowOffset: { width: 0, height: 0 }, 
		shadowOpacity: 0.2, 
        shadowRadius: 8,
        overflow: 'visible',
        borderRadius: 10,
    },
    swipeOut: {
        backgroundColor: 'white',
        borderRadius: 10,
    },
    titleWrapper: {
        width: '55%',
        marginRight: 5,
    },
    title: {
        color: 'black',
        fontSize: 20,
        fontWeight: '500',
    },
    wrapper: {
        paddingHorizontal: 20,
        marginVertical: 10,
        width: '100%',
    },
})

export const favCardStyles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        padding: 15,
        borderRadius: 10,
        width: 140,
        height: 130,
        marginLeft: 20,
        marginVertical: 10,
        elevation: 5,
        zIndex: 5,
        shadowColor: "#000", 
		shadowOffset: { width: 0, height: 0 }, 
		shadowOpacity: 0.2, 
		shadowRadius: 8,
    },
    content: {
        flex: 1,
        justifyContent: 'space-between'
    },
    formula: {
        fontSize: 15,
        color: 'black'
    },
    favButton: {
        textAlign: 'right',
        color: theme.color.shade,
        fontSize: 25,
    },
    navButton: {
        flexDirection: 'row',
        marginTop: 10,
    },
    navButtonTitle: {
        color: theme.color.shade,
        fontSize: 12,
    },
    navButtonIcon: {
        alignSelf: 'flex-end',
        color: theme.color.shade,
        fontSize: 12,
        marginLeft: 10,
        top: 2.5
    },
    title: {
        fontWeight: '600',
        color: 'black',
        fontSize: 18
    },
})