import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import { Animated, View, Text, TouchableOpacity, TextInput } from 'react-native';

import { navbarStyles as styles } from './component.style';
import { compose, withState } from 'recompose';
import { theme } from '@assets/style';

const ListChaptersNavbar = props => {
    return (
        <View style={styles.navBar} >
            <TouchableOpacity
                onPress={props.handleBack}
                onLongPress={props.handleLongPressBack}
            >
                <Icon name="arrow-left" style={styles.back} size={30} />
            </TouchableOpacity>
            {props.searchBar ? 
                <Animated.View
                    style={styles.searchInputWrapper}
                >
                    <TextInput 
                        onChangeText={(input) => {
                            props.setSearchValue(input)
                            props.handleSearchDictionary(input)
                        }}
                        value={props.searchValue}
                        onFocus={() => props.searchValue == 'Search' && props.setSearchValue('')}
                        onBlur={() => props.searchValue == '' && props.setSearchValue('Search')}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        autoCorrect={false}
                        style={styles.searchInput} />
                </Animated.View>
                : 
                <Text style={styles.title} >{props.title}</Text>
            }
            {props.formFactor == 'dictionary' ?
                <TouchableOpacity
                    onPress={() => props.setSearchBar(!props.searchBar)}
                >
                    <Icon name="search" style={styles.search} size={30} />
                </TouchableOpacity>
                :
                <View>
                    <Icon name="square" style={styles.search} size={30} />
                </View>
            }
        </View>
    )
}

export default compose(
    withState('searchBar', 'setSearchBar', false),
    withState('searchValue', 'setSearchValue', 'Search')
)(ListChaptersNavbar);