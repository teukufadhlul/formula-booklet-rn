import React from 'react';
import Swipeout from 'react-native-swipeout';
import Icon from 'react-native-vector-icons/FontAwesome';
import { View, TouchableWithoutFeedback, Text, Image } from 'react-native';

import { compose, withHandlers, withState, lifecycle } from 'recompose';

import { theme } from '@assets/style';
import { listChaptersCardStyles as styles } from './component.style';

const swipeoutBtns = [
    { 
        text: 'Add Fav',
        backgroundColor: theme.color.secondary,
    }
]

const ListChaptersCard = props => {
    return (
        <View style={styles.wrapper} >
            <View style={styles.swipeContainer} >
                <Swipeout 
                    autoClose={true}
                    close={props.closeSwipeout}
                    onOpen={(sectionID, rowId, direction) => {
                        props.setScrollEnabled(false)
                        if (sectionID == -1 && rowId == -1 && direction == "right"){
                            props.handleSwipeEvent()
                        }
                    }}
                    onClose={(sectionID, rowId, direction) => {
                        props.setScrollEnabled(true)
                    }}
                    right={swipeoutBtns} 
                    style={styles.swipeOut}>
                        <TouchableWithoutFeedback
                            onPress={props.handlePress} >
                            <View style={styles.container} >
                                <View style={styles.content} >
                                    <View style={[styles.titleWrapper, {width: props.data.word ? '100%' : '55%'}]} >
                                        <Text style={styles.title} >{props.data.title || props.data.word}</Text>
                                        {props.data.formula &&
                                            <Text style={styles.formula} >{props.data.formula[0].formula}</Text>
                                        }
                                        <Text style={styles.subtitle } >{props.data.subtitle || props.data.definition}</Text>
                                        <Text style={styles.readMore} >Lebih Lanjut</Text>
                                    </View>
                                    {props.data.image && 
                                        <View style={styles.imageWrapper} >
                                            <Image style={styles.image} source={props.data.image} />
                                        </View>    
                                    }
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </Swipeout>
            </View>
        </View>
    )
}

export default compose(
    withState('closeSwipeout', 'setCloseSwipeout', false),
    withHandlers({
        handlePress: props => () => {
            props.onPressCard(props.data)
        },
        handleSwipeEvent: props => () => {
            props.setCloseSwipeout(true)
            props.setScrollEnabled(false)
            props.onSwipeEvent(props.data)
        }
    }),
    lifecycle({
        componentDidUpdate(){
            if (this.props.closeSwipeout){
                this.props.setCloseSwipeout(false)
            }
        }
    }),
)(ListChaptersCard);