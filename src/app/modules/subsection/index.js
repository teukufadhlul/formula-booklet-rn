import React from 'react';
import { 
    View, Text, FlatList, ScrollView, ImageBackground, 
    Platform, ActivityIndicator, Vibration, UIManager, 
    findNodeHandle } from 'react-native';

import { connect } from 'react-redux';
import { SCREENS } from '@app/navigation/screens';
import { Navigation } from 'react-native-navigation';
import { addHistory, addFavourites } from '@app/redux/action';
import { enhanceSubsection } from '@domain/container/subsection';
import { compose, withProps, withHandlers, withState, lifecycle } from 'recompose';

import { updateSubsection } from '@data/realm/queries/subsection'
import { getRandomSentence, getUrbanSentence } from '@app/redux/action'

import { ListChaptersCard, ListChaptersNavbar, FavCard } from './component';
import { styles } from './index.style';
import { theme } from '@assets/style';

const Subsection = props => { 

    return(
        <View style={styles.wrapper} >
            <View style={styles.imageBackgroundContainer} > 
                <ImageBackground
                    source={props.bgImage}
                    style={styles.imageBackground}
                    blurRadius={Platform.OS === 'ios' ? 5 : 2 }
                />
            </View>


            <ListChaptersNavbar 
                title={props.title} 
                handleBack={props.handleBack} 
                handleLongPressBack={props.handleLongPressBack}
                formFactor={props.formFactor}
                handleSearchDictionary={props.handleSearchDictionary}
            />

            <ScrollView
                scrollEnabled={props.scrollEnabled} >
                { props.formFactor !== 'dictionary' && (
                        <View style={styles.favMaterial}>
                            <Text style={styles.favTitle}>
                                Materi Favorit
                            </Text>
                            { props.subsectionFav.length > 0 &&
                                <FlatList
                                    showsHorizontalScrollIndicator={false}
                                    horizontal
                                    data={props.subsectionFav}
                                    keyExtractor={(item, index) => index.toString()}
                                    extraData={props.refreshFav}
                                    renderItem={({item, index}) => (
                                        <FavCard data={item} />
                                    )}
                                />
                            }
                        </View>
                    )
                }
                <View
                    ref={_ref => props.setViewRef(_ref)}
                    onLayout={({nativeEvent}) => {
                        props.handleViewLayout()
                    }}
                    style={styles.allMaterial} >
                    { props.formFactor !== 'dictionary' &&
                        <Text style={[
                            styles.subtitle, {
                                color: props.titleColor,
                                textShadowColor: props.titleColor == 'white' ? 'rgba(0, 0, 0, 0.5)' : 'white',
                                textShadowOffset: props.titleColor == 'white' ? {width: -1, height: -1} : {width: 0, height: 0},
                                textShadowRadius: props.titleColor == 'white' ? 10 : 0
                            }]}>
                            Semua Materi
                        </Text>
                    }
                    { props.interaction ? 
                        <FlatList
                            scrollEnabled={false}
                            data={props.formFactor !== 'dictionary' ? props.data : props.urbanSentence.list}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({item, index}) => (
                                <ListChaptersCard 
                                    data={item}
                                    title={item.word}
                                    setScrollEnabled={props.setScrollEnabled}
                                    onSwipeEvent={props.onSwipeEvent}
                                    onPressCard={props.onPressCard} />
                            )}
                        />
                    :
                        <ActivityIndicator style={styles.activityIndicator} size="large" color={theme.color.shade} />
                    }
                </View >
            </ScrollView>
        </View>
    )
}

const mapStateToProps = state => ({
    subsectionHistory: state.subsectionHistory.history,
    subsectionFav: state.subsectionFav.favourites,
    urbanSentence: state.getUrbanSentence
})

export default enhanceSubsection(compose(
    connect(mapStateToProps),
    withState('interaction', 'setInteraction', false),
    withState('scrollEnabled', 'setScrollEnabled', true),
    withState('refreshFav', 'setRefreshFav', false),
    withState('titleColor','setTitleColor', 'black'),
    withProps(props => {
        return ({
            onPressCard: (data) => {

                props.dispatch(addHistory(data, props.subsectionHistory))
                
                Navigation.push(props.componentId, {
                    component: {
                        name: SCREENS.COURSE,
                        passProps: {
                            data: data,
                        },
                        options: {
                            topBar: {
                                visible: false,
                                drawBehind: true,
                                animate: false
                            },
                            bottomTabs: { 
                                visible: false, 
                                drawBehind: true, 
                                animate: true 
                            } 
                        }
                    }
                });
            },
            onSwipeEvent: data => {
                props.dispatch(addFavourites(data, props.subsectionHistory))
                props.setRefreshFav(!props.refreshFav)
            },
            onSearchDictionary: word => {
                // if (word.length % 3 == 0){
                    if (word == '') {
                        props.dispatch(getRandomSentence())
                    } else {
                        props.dispatch(getUrbanSentence(word))
                    }
                // }
            }
        })
    }),
    withHandlers(() => {
        let viewRef;
        return {
            setViewRef: props => _ref =>{
                viewRef = _ref
            },
            handleViewLayout: props => () => {
                let view = viewRef
                let handle = findNodeHandle(view);
                // UIManager.measure(handle, (x, y, width, height, pageX, pageY) => {
                //     if (props.titleColor == 'black' && y < 200) {
                //         props.setTitleColor('white')
                //     } else if (props.titleColor == 'white' && y > 200) {
                //         props.setTitleColor('black')
                //     }
                // })
            },
            handleBack: props => () => {
                Navigation.pop(props.componentId);
            },
            handleLongPressBack: props => () => {
                Vibration.vibrate(100)
                Navigation.popToRoot(props.componentId)
            },
            handleSearchDictionary: props => word => {
                props.onSearchDictionary(word)
            }
        }
    }),
    lifecycle({
        componentDidMount() {
            this.navigationEventListener = Navigation.events().bindComponent(this);
            this.props.setInteraction(true)
            this.props.dispatch(getRandomSentence())
        },
        componentWillUnmount() {
            if (this.navigationEventListener) {
                this.navigationEventListener.remove();
            }
        },
        componentDidAppear() {
            this.props.setRefreshFav(!this.props.refresh)
        },
        componentDidDisappear() {
            // Navigation.setComponentId(this.props.componentId)
        }
    })
)(Subsection));