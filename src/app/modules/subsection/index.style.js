import { StyleSheet } from 'react-native';

import { isIphoneX, isScreen16, height, width } from '@domain/phoneLayout'

export const styles = StyleSheet.create({
    allMaterial: {
        marginBottom: 10,
        marginTop: 20,
    },
    activityIndicator: { 
        alignSelf: 'center', 
        justifyContent: 'center', 
        height: '100%'
    },
    favMaterial: {

    },
    favTitle: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        fontSize: 25,
        color: 'white',
        fontWeight: '600',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10
    },
    imageBackgroundContainer: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        borderBottomLeftRadius: 200,
        borderBottomRightRadius: 200,
        overflow: 'hidden',
        transform: [
            {scaleX: 1.6}
        ],
        bottom: isIphoneX() ? height * 0.65 : height * 0.58
    },  
    imageBackground: {
        width: '100%',
        height: '100%',
        bottom: -40
    },
    subtitle: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        fontSize: 25,
        color: 'black',
        fontWeight: '600',
    },
    
    wrapper: {
        height: '100%',
        backgroundColor: '#FFFF',
        paddingTop: isIphoneX() ? 35 : 10,
        width: '100%',
        overflow: 'hidden',
    },
    
})