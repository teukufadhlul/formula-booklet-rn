export { default as About } from './about';
export { default as Calculator } from './calculator';
export { default as ELearning } from './e-learning';
export { default as Lessons } from './lessons';
export { default as History } from './history';
export { default as Section } from './section';
export { default as Subsection } from './subsection';
export { default as Course } from './course';