
import { initNavigation } from '@app/navigation';
import Reactotron from 'reactotron-react-native'

import { IGNORE_YELLOW_WARNING } from '@config/development'

export const App = () => {
    if (__DEV__){
        IGNORE_YELLOW_WARNING
        
        Reactotron
            .configure() // controls connection & communication settings
            .useReactNative() // add all built-in react native plugins
            .connect() // let's connect!

        console.tron = Reactotron.log

    }
    initNavigation()
}