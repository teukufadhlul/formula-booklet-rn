export const customTopBar = {
    visible: false,
    drawBehind: true,
    hideOnScroll: false,
    elevation: 0,
    background: {
        color: 'transparent'
    }
}

export const navAnimations = {
    push: { //rtl
        waitForRender: true,
        content: {
            x: {
                from: 1000,
                to: 0,
                duration: 300,
                // startDelay: 100,
                interpolation: 'decelerate'
            },
            alpha:{
                from: 1,
                to: 1,
            }
        }
    },
    pop: { //rtl
        content: {
            x: {
                from: 0,
                to: 1000,
                duration: 300,
                // startDelay: 100,
                interpolation: 'accelerate'
            },
            alpha:{
                from: 1,
                to: 1,
            }
        }
    },
}