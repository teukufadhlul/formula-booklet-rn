import { customTopBar, navAnimations } from './config';
import { SCREENS } from '@app/navigation/screens';

import { imgCollege } from '@assets/bottom-bar';
import { theme } from '@assets/style';

export const navOptionsELearning = {
    stack: {
        options: {
            layout: {
                backgroundColor: 'white'
            },
            animations: navAnimations,                                        
            bottomTab: {
                iconColor: theme.color.bottomBar,
                textColor: theme.color.bottomBar,
                selectedIconColor: theme.color.bottomBarActive,
                selectedTextColor: theme.color.bottomBarActive,
            }
        },
        children: [
            {
                component: {
                    name: SCREENS.E_EARNING,
                    options: {
                        topBar: customTopBar,
                        bottomTab: {
                            text: 'E-Learning',
                            icon: imgCollege,
                            iconColor: theme.color.bottomBar,
                            textColor: theme.color.bottomBar,
                            selectedIconColor: theme.color.bottomBarActive,
                            selectedTextColor: theme.color.bottomBarActive,
                        }
                    }
                }
            },
        ]
    }
}