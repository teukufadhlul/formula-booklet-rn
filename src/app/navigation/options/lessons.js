import { navAnimations, customTopBar } from './config';
import { SCREENS } from '@app/navigation/screens';

import { imgMolecule } from '@assets/bottom-bar';
import { theme } from '@assets/style';

export const navOptionsLessons = {
    stack: {
        options: {
            layout: {
                backgroundColor: 'white'
            },
            animations: navAnimations,                                        
            bottomTab: {
                iconColor: theme.color.bottomBar,
                textColor: theme.color.bottomBar,
                selectedIconColor: theme.color.bottomBarActive,
                selectedTextColor: theme.color.bottomBarActive,
            }
        },
        children: [
            {
                component: {
                    name: SCREENS.LESSONS,
                    options: {
                        topBar: customTopBar,
                        bottomTab: {
                            text: 'Lessons',
                            icon: imgMolecule,
                            iconColor: theme.color.bottomBar,
                            textColor: theme.color.bottomBar,
                            selectedIconColor: theme.color.bottomBarActive,
                            selectedTextColor: theme.color.bottomBarActive,
                        }
                    }
                }
            },
        ]
    }
}