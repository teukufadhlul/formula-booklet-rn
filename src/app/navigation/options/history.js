import { customTopBar, navAnimations } from './config';
import { SCREENS } from '@app/navigation/screens';

import { imgHistory } from '@assets/bottom-bar';
import { theme } from '@assets/style';

export const navOptionsHistory = {
    stack: {
        options: {
            layout: {
                backgroundColor: 'white'
            },
            animations: navAnimations,                                        
            bottomTab: {
                iconColor: theme.color.bottomBar,
                textColor: theme.color.bottomBar,
                selectedIconColor: theme.color.bottomBarActive,
                selectedTextColor: theme.color.bottomBarActive,
            }
        },
        children: [
            {
                component: {
                    name: SCREENS.HISTORY,
                    options: {
                        topBar: customTopBar,
                        bottomTab: {
                            text: 'History',
                            icon: imgHistory,
                            iconColor: theme.color.bottomBar,
                            textColor: theme.color.bottomBar,
                            selectedIconColor: theme.color.bottomBarActive,
                            selectedTextColor: theme.color.bottomBarActive,
                        }
                    }
                }
            },
        ]
    }
}