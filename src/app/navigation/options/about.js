import { customTopBar, navAnimations } from './config';
import { SCREENS } from '@app/navigation/screens';

import { imgUser } from '@assets/bottom-bar';
import { theme } from '@assets/style';

export const navOptionsAbout = {
    stack: {
        options: {
            layout: {
                backgroundColor: 'white'
            },
            animations: navAnimations,                                        
            bottomTab: {
                iconColor: theme.color.bottomBar,
                textColor: theme.color.bottomBar,
                selectedIconColor: theme.color.bottomBarActive,
                selectedTextColor: theme.color.bottomBarActive,
            }
        },
        children: [
            {
                component: {
                    name: SCREENS.ABOUT,
                    options: {
                        topBar: customTopBar,
                        bottomTab: {
                            text: 'About',
                            icon: imgUser,
                            iconColor: theme.color.bottomBar,
                            textColor: theme.color.bottomBar,
                            selectedIconColor: theme.color.bottomBarActive,
                            selectedTextColor: theme.color.bottomBarActive,
                        }
                    }
                }
            },
        ]
    }
}