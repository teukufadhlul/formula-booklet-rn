import { navOptionsLessons } from './lessons';
import { navOptionsELearning } from './eLearning';
import { navOptionsHistory } from './history';
import { navOptionsAbout } from './about';

import { theme } from '@assets/style';

export const navOptionsRoot = {
    root: {
        bottomTabs: {
            options: {
                bottomTabs: {
                    titleDisplayMode: 'alwaysShow',
                    currentTabIndex: 0,
                },
                bottomTab: {
                    iconColor: theme.color.bottomBar,
                    textColor: theme.color.bottomBar,
                    selectedIconColor: theme.color.bottomBarActive,
                    selectedTextColor: theme.color.bottomBarActive,
                },
                overlay: {
                    interceptTouchOutside: true
                },
            },
            children: [
                navOptionsLessons,
                navOptionsHistory,
                navOptionsELearning,
                navOptionsAbout
            ],
        }
    }
}