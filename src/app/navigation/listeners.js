import { Navigation } from 'react-native-navigation';
import { SCREENS } from './screens';

import { navOptionsRoot } from './options'

import { DEFAULT_SCREEN, DEV_MODE } from '@config/development';

export const registerListeners = () => {
    
    Navigation.events().registerAppLaunchedListener(() => {
		if ( DEV_MODE ) {
			Navigation.setRoot({
				root: {
					component: {
					name: DEFAULT_SCREEN,
					},
				},
			});
		} else {
			Navigation.setRoot(navOptionsRoot);
		}
    });
}