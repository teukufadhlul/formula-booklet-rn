import { registerComponent } from './reduxHelpers';

import { About, Calculator,
        ELearning, Lessons, History,
        Section, Subsection, Course
} from '@app/modules';

import { AdsModal } from '@app/components';

export const SCREENS = {
    ABOUT: 'AboutScreen',
    CALCULATOR: 'CalculatorScreen',
    E_EARNING: 'ELearningScreen',
    LESSONS: 'LessonsScreen' ,
    HISTORY: 'HistoryScreen',
    SECTION: 'SectionScreen',
    SUBSECTION: 'SubsectionScreen',
    COURSE: 'CourseScreen',
    ADS_MODAL: 'AdsModalScreen'
  };

export const registerScreen  = () => {
    registerComponent(SCREENS.ABOUT, About);
    registerComponent(SCREENS.CALCULATOR, Calculator);
    registerComponent(SCREENS.E_EARNING, ELearning);
    registerComponent(SCREENS.LESSONS, Lessons);
    registerComponent(SCREENS.HISTORY, History);
    registerComponent(SCREENS.SECTION, Section);
    registerComponent(SCREENS.SUBSECTION, Subsection);
    registerComponent(SCREENS.COURSE, Course);
    registerComponent(SCREENS.ADS_MODAL, AdsModal);
}
