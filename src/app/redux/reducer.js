
import { combineReducers } from 'redux';

import {
    userConfig,
    subsectionHistory,
    subsectionFav,
    getUrbanSentence
} from './reducers';

const reducer = combineReducers({
    userConfig,
    subsectionFav,
    getUrbanSentence,
    subsectionHistory
});

export default reducer;