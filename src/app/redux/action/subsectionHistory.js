export const addHistory = (payload, previousState) => {

    previousState.forEach( (item, index) => {
        if (previousState.length > 50){
            previousState.pop()
        }

        if (payload.id == item.id) {
            previousState.splice(index,1)
        }
    })

    payload.createdAt = new Date()

    previousState.unshift(payload)

    return {
        type: "ADD_HISTORY",
        payload: previousState
    }
}

export const removeHistory = (previousState, index) => {

    previousState.splice(index, 1)

    return {
        type: "REMOVE_HISTORY",
        payload: previousState
    }

}