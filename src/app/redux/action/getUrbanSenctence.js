import axios from 'axios'
import Reactotron from 'reactotron-react-native';

export const getUrbanSentence = (sentence) => {
    return {
        type: 'GET_URBAN',
        payload: axios({
            method: 'GET',
            url: `https://api.urbandictionary.com/v0/define?term=${sentence}`
        })
    }
}

export const getRandomSentence = () => {    
    Reactotron.log("It's called")
    return {
        type: 'GET_URBAN',
        payload: axios({
            method: 'GET',
            url: `https://api.urbandictionary.com/v0/random`
        })
    }
}