export const addFavourites = (payload, previousState) => {
    previousState.forEach( (item, index) => {
        if (previousState.length > 10){
            previousState.pop()
        }

        if (payload.id == item.id) {
            previousState.splice(index,1)
        }
    })

    payload.createdAt = new Date()

    previousState.unshift(payload)

    return {
        type: "ADD_FAVOURITES",
        payload: previousState
    }
}

export const removeFavourites = (previousState, index) => {

    previousState.splice(index, 1)

    return {
        type: "REMOVE_FAVOURITES",
        payload: previousState
    }

}