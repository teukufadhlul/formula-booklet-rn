//Urban Dicttonary reducer

const initialState = {
    list: [],
    isLoading: false,
    error: false
}

const getUrbanSentence = ( state = initialState, action ) => {
    switch (action.type) {
        case 'GET_URBAN_PENDING':
            // console.tron(action.payload)
            return {
                ...state,
                isLoading: true,
                error: false
            }
        case 'GET_URBAN_FULFILLED':
            // console.tron(action.payload) 
            return {
                ...state,
                list: action.payload.data.list || [],
                isLoading: false,
                error: false
            }
        case 'GET_URBAN_REJECTED':
            // console.tron(action.payload) 
            return {
                isLoading: false,
                error: action.payload
            }
        default:
            return state
    }
}

export default getUrbanSentence;