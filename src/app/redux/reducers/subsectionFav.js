const initialState = {
    favourites: []
}

const subsectionFav = ( state = initialState, action ) => {
    switch (action.type) {
        case "ADD_FAVOURITES":
            return {
                ...state,
                favourites: action.payload || []
            }
        case "REMOVE_FAVOURITES":
            return {
                ...state,
                favourites: action.payload || []
            }
        case "REMOVE_ALL_FAVOURITES":
            return {
                ...state,
                favourites: []
            }
        default:
            return state;
    }
}

export default subsectionFav;