const initialState = {
    history: []
};

const subsectionHistory = ( state = initialState, action ) => {
    switch (action.type) {
        case 'ADD_HISTORY':
            return {
                ...state,
                history: action.payload || []
            }
        case 'REMOVE_HISTORY': 
            return {
                ...state,
                history: action.payload || []
            }
        case 'REMOVE_ALL_HISTORY': 
            return {
                history: []
            }
        default:
            return state
    }
}

export default subsectionHistory;