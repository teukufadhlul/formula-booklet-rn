import { createLogger } from 'redux-logger';
import { LOGGER } from '@config/development';
import promise from 'redux-promise-middleware';

const logger =  createLogger()

const middlewares = [
    promise
];

if (__DEV__ && LOGGER) {
    middlewares.push(logger)
} 

export default middlewares;