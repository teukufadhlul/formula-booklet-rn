import React from 'react';
import { Image, View, Dimensions, BackHandler } from 'react-native';

import { lifecycle, compose } from 'recompose';

const AdsModal = props => {
    const getRandomInt = (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }
    const  getBackupAds = () => {
        let uri;
        uri = props.fallbackAds[getRandomInt(0, props.fallbackAds.length)]
    
        return uri;
      }
    return(
        <View
        style={{
            backgroundColor: 'white',
            height: '100%',
            width: '100%'
          }} >
            <Image
                source={{uri: getBackupAds()}}
                style={{width: Dimensions.get('window').width, height: Dimensions.get('window').height}} />
        </View>
    )
}

export default compose(
    lifecycle({
        componentDidMount() {
            BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
          },
          componentWillUnmount() {
            BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
          },
          onBackPress() {
            return true;
          },
    })
)(AdsModal);
