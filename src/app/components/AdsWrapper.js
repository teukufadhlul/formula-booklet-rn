import React, {  Component } from 'react';
import { View, Platform } from 'react-native';

import firebase from 'react-native-firebase';
import { Navigation } from 'react-native-navigation';

import { SCREENS } from '@app/navigation/screens';

const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}
export default class AdsWrapper extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false
        }
    }

    getLevelRewardAdsId = ({ ios, android }) => {
        const dev = 'ca-app-pub-3940256099942544/5224354917'

        if (__DEV__) return dev
        return Platform.OS === 'ios' ? ios : android
    }

    getBackupAds() {
        let uri;
        uri = this.props.fallbackAds[getRandomInt(0, this.props.fallbackAds.length)]

        return uri;
    }

    requestNewAd() {
        this.advert = firebase.admob().rewarded(this.getLevelRewardAdsId(this.props.googleMobAds))
        const request = new firebase.admob.AdRequest()

        this.advert.on('onAdFailedToLoad', (event) => {
            // console.tron(event)
        })

        this.advert.on('onAdClosed', (event) => {
            setTimeout(() => this.requestNewAd(), 1000)
            this.props.onAdClosed();
        })

        this.advert.on('onRewarded', (event) => {
            this.props.onWatched();
        });

        this.advert.loadAd(request.build())
    }

    showNativeModal() {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: SCREENS.ADS_MODAL,
                        passProps: {
                            fallbackAds: this.props.fallbackAds,
                        },
                        options: {
                            topBar: {
                                visible: false,
                                drawBehind: true,
                                animate: false
                            },
                            bottomTabs: {
                                visible: false,
                                drawBehind: true,
                                animate: true
                            }
                        }
                    }
                }]
            }
        });
    }

    hideNativeModal() {
        Navigation.dismissAllModals();
    }

    show() {
        if (this.advert.isLoaded())
            this.advert.show();
        else
            this.showBackupAds()
    }

    showBackupAds() {
        this.showNativeModal()
        setTimeout(() => {
            this.hideNativeModal()
            this.props.onWatched();
        }, 3000);
    }

    componentWillMount() {
        this.requestNewAd();
    }

    render() {
        return (
            <View>
                {this.props.children(this.show.bind(this))}
            </View>
        );
    }
};