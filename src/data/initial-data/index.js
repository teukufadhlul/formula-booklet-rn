let i = 1

export const initialData = [
    {
        id: Math.floor(Date.now() / i++),
        subjects: "Fisika",
        description: "Pengertian fisika yaitu berasal dari kata “physic” yang artinya yaitu alam. Jadi ilmu fisika yaitu sebuah ilmu pengetahuan dimana didalamnya mempelajari tentang sifat dan fenomena alam atau gejala alam dan seluruh interaksi yang terjadi didalamnya. Untuk mempelajari fenomena atau gejala alam, fisika menggunakan proses dimulai dari pengamatan, pengukuran, analisis dan menarik kesimpulan. Sehingga prosesnya lama dan berbuntut panjang, namun hasilnya bisa dipastikan akurat karena fisika termasuk ilmu eksak yang kebenarannya terbukti.",
        author: "Wikipedia",
        image:"karol_homepage",
        filter: [
            {
                id: Math.floor(Date.now() / i++ ),
                filter: 'All',
            },
            {
                id: Math.floor(Date.now() / i++ ),
                filter: 'Gaya',
            },
            {
                id: Math.floor(Date.now() / i++ ),
                filter: 'Alat Optik',
            },
            {
                id: Math.floor(Date.now() / i++ ),
                filter: 'Termodinamika',
            },
            {
                id: Math.floor(Date.now() / i++ ),
                filter: 'Pesawat Sederhana'
            }
        ],
        section: [
            {
                id: Math.floor(Date.now() / i++),
                category: 'Mekanika',
                title: 'Kinematika Gerak',
                subtitle: 'Dalam fisika, kinematika adalah cabang dari mekanika klasik yang membahas gerak benda dan sistem benda tanpa mempersoalkan gaya penyebab gerakan.',
                image: 'kinematika_gerak',
                formFactor: 'default',
                subsection: [
                    {
                        id: Math.floor(Date.now() / i++),
                        isFavorite: true,
                        title: 'Gerak Lurus',
                        subtitle: 'Gerak Lurus Beraturan (GLB) adalah suatu gerak lurus yang mempunyai kecepatan konstan.',
                        image: 'undraw_logistics',
                        inventor: 'galileo galilei',
                        inventorImg: 'galileo_galilei',
                        explanation: 'Gerak Lurus Beraturan (GLB) adalah suatu gerak lurus yang mempunyai kecepatan konstan. Maka nilai percepatannya adalah a = 0. Gerakan GLB berbentuk linear dan nilai kecepatannya adalah hasil bagi jarak dengan waktu yang ditempuh.',
                        formula: [
                            {
                                id: Math.floor(Date.now() / i++),
                                formula: 's = v . t',
                                argument: 'string'
                            }                 
                        ]
                    },
                    {
                        id: Math.floor(Date.now() / i++),
                        isFavorite: true,
                        title: 'GLBB Horizontal',
                        subtitle: 'Gerak yang lintasannya berupa garis lurus dengan kecepatannya yang berubah beraturan.',
                        image: 'undraw_target',
                        inventor: 'galileo galilei',
                        inventorImg: 'galileo_galilei',
                        explanation: 'Gerak lurus berubah beraturan adalah gerak yang lintasannya berupa garis lurus dengan kecepatannya yang berubah beraturan. \n Percepatannya bernilai konstan/tetap.',
                        formula: [
                            {
                                id: Math.floor(Date.now() / i++),
                                formula: 'Vt = Vo + a . t',
                                argument: 'string'
                            },                          
                            {
                                id: Math.floor(Date.now() / i++),
                                formula: 'Vt ^2 = Vo ^2 + 2 . a . s',
                                argument: 'string'
                            },                          
                            {
                                id: Math.floor(Date.now() / i++),
                                formula: 's = Vo . t + 1/2 . a . t ^2',
                                argument: 'string'
                            },                          
                        ]
                    },
                    {
                        id: Math.floor(Date.now() / i++),
                        isFavorite: true,
                        title: 'Gerak Vertikal Ke atas',
                        subtitle: 'Arah gerak benda dan arah percepatan gravitasi berlawana.',
                        image: 'undraw_rocket',
                        inventor: 'galileo galilei',
                        inventorImg: 'galileo_galilei',
                        explanation: 'Benda dilemparkan secara vertikal, tegak lurus terhadap bidang horizontal ke atas dengan kecepatan awal tertentu. Arah gerak benda dan arah percepatan gravitasi berlawanan, gerak lurus berubah beraturan diperlambat.',
                        formula: [
                            {
                                id: Math.floor(Date.now() / i++),
                                formula: 'tmax = Vo / g',
                                argument: 'string'
                            },                          
                            {
                                id: Math.floor(Date.now() / i++),
                                formula: 'h = Vo ^2 / 2 . g',
                                argument: 'string'
                            },                          
                            {
                                id: Math.floor(Date.now() / i++),
                                formula: 't = 2 . tmax',
                                argument: 'string'
                            },                          
                            {
                                id: Math.floor(Date.now() / i++),
                                formula: 'Vt ^2 = Vo ^2 - 2 . g . h',
                                argument: 'string'
                            },                          
                        ]
                    }
                ]
            }
        ]
    },
    {
        id: Math.floor(Date.now() / i++),
        subjects: "Kimia",
        description: "English is a West Germanic language that was first spoken in early medieval England and eventually became a global lingua franca. It is named after the Angles, one of the Germanic tribes that migrated to the area of Great Britain that later took their name, as England. Both names derive from Anglia, a peninsula in the Baltic Sea. The language is closely related to Frisian and Low Saxon, and its vocabulary has been significantly influenced by other Germanic languages, particularly Norse (a North Germanic language), and to a greater extent by Latin and French.",
        author: "Wikipedia",
        image:"design_party",
        section: []
    },
    // {
    //     id: Math.floor(Date.now() / i++),
    //     subjects: "English",
    //     description: "English is a West Germanic language that was first spoken in early medieval England and eventually became a global lingua franca. It is named after the Angles, one of the Germanic tribes that migrated to the area of Great Britain that later took their name, as England. Both names derive from Anglia, a peninsula in the Baltic Sea. The language is closely related to Frisian and Low Saxon, and its vocabulary has been significantly influenced by other Germanic languages, particularly Norse (a North Germanic language), and to a greater extent by Latin and French.",
    //     author: "Wikipedia",
    //     image:"design_party",
    //     filter: [
    //         {
    //             id: Math.floor(Date.now() / i++ ),
    //             filter: 'All',
    //         },
    //         {
    //             id: Math.floor(Date.now() / i++ ),
    //             filter: 'Grammar',
    //         },
    //         {
    //             id: Math.floor(Date.now() / i++ ),
    //             filter: 'Idioms',
    //         }
    //     ],
    //     section: [
    //         {
    //             id: Math.floor(Date.now() / i++),
    //             category: 'grammar',
    //             title: 'All 16 tenses',
    //             subtitle: 'Dalam fisika, kinematika adalah cabang dari mekanika klasik yang membahas gerak benda dan sistem benda tanpa mempersoalkan gaya penyebab gerakan.',
    //             image: 'kinematika_gerak',
    //             formFactor: 'default',
    //             subsection: [
    //                 {
    //                     id: Math.floor(Date.now() / i++),
    //                     isFavorite: true,
    //                     title: 'Gerak Lurus',
    //                     subtitle: 'Gerak Lurus Beraturan (GLB) adalah suatu gerak lurus yang mempunyai kecepatan konstan.',
    //                     image: 'undraw_logistics',
    //                     inventor: 'galileo galilei',
    //                     inventorImg: 'galileo_galilei',
    //                     explanation: 'Gerak Lurus Beraturan (GLB) adalah suatu gerak lurus yang mempunyai kecepatan konstan. Maka nilai percepatannya adalah a = 0. Gerakan GLB berbentuk linear dan nilai kecepatannya adalah hasil bagi jarak dengan waktu yang ditempuh.',
    //                     formula: [
    //                         {
    //                             id: Math.floor(Date.now() / i++),
    //                             formula: 's = v . t',
    //                             argument: 'string'
    //                         }                 
    //                     ]
    //                 }
    //             ]
    //         },
    //         {
    //             id: Math.floor(Date.now() / i++),
    //             category: 'Idioms',
    //             title: 'Urban Dictionary',
    //             subtitle: 'Dalam fisika, kinematika adalah cabang dari mekanika klasik yang membahas gerak benda dan sistem benda tanpa mempersoalkan gaya penyebab gerakan.',
    //             image: 'urban_dictionary',
    //             formFactor: 'dictionary',
    //             subsection: [
    //                 {
    //                     id: Math.floor(Date.now() / i++),
    //                     isFavorite: true,
    //                     title: 'Gerak Lurus',
    //                     subtitle: 'Gerak Lurus Beraturan (GLB) adalah suatu gerak lurus yang mempunyai kecepatan konstan.',
    //                     image: 'undraw_logistics',
    //                     inventor: 'galileo galilei',
    //                     inventorImg: 'galileo_galilei',
    //                     explanation: 'Gerak Lurus Beraturan (GLB) adalah suatu gerak lurus yang mempunyai kecepatan konstan. Maka nilai percepatannya adalah a = 0. Gerakan GLB berbentuk linear dan nilai kecepatannya adalah hasil bagi jarak dengan waktu yang ditempuh.',
    //                     formula: [
    //                         {
    //                             id: Math.floor(Date.now() / i++),
    //                             formula: 's = v . t',
    //                             argument: 'string'
    //                         }                 
    //                     ]
    //                 }
    //             ]
    //         }
    //     ]
    // },
]