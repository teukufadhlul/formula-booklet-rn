import Realm from 'realm';

import { databaseOptions } from '@data/realm';
import { HISTORY_SCHEMA } from '@data/realm/schema/history';

export const getAllHistories = () => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        let allHistories = realm.objects(HISTORY_SCHEMA)
        resolve(allHistories)
    }).catch(err => {
        reject(err)
    })
})

export const writeHistory = newHistory => new Promise((resolve, reject) => {
    if (newHistory instanceof Array){
        Realm.open(databaseOptions).then(realm => {
            realm.write(() => {
                newHistory.forEach((item) => {
                    realm.create(HISTORY_SCHEMA, item);
                    resolve(item)
                })
            })
        }).catch(err => {
            reject(err)
        })
    } else if (newHistory instanceof Object) {
        Realm.open(databaseOptions).then(realm => {
            realm.write(() => {
                realm.create(HISTORY_SCHEMA, newHistory);
                resolve(newHistory)
            })
        }).catch(err => {
            reject(err)
        })
    }
})

export const updateHistory = ( newHistory ) => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            realm.create(HISTORY_SCHEMA, newHistory);
            resolve(newHistory)
        }).catch(err => {
            reject(err)
        })
    })
})

export const deleteAllHistories = obj => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            realm.delete(realm.objects(obj))
            resolve(null)
        })
    }).catch(err => {
        reject(err)
    })
})