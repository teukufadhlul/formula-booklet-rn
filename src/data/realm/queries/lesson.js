import Realm from 'realm';

import { databaseOptions } from '@data/realm';
import { LESSON_SCHEMA } from '@data/realm/schema/lesson';

export const getAllLessons = () => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        let allLessons = realm.objects(LESSON_SCHEMA)
        resolve(allLessons)
    }).catch(err => {
        reject(err)
    })
})

export const writeLesson = newLesson => new Promise((resolve, reject) => {
    if (newLesson instanceof Array){
        Realm.open(databaseOptions).then(realm => {
            realm.write(() => {
                newLesson.forEach((item) => {
                    realm.create(LESSON_SCHEMA, item);
                    resolve(item)
                })
            })
        }).catch(err => {
            reject(err)
        })
    } else if (newLesson instanceof Object) {
        Realm.open(databaseOptions).then(realm => {
            realm.write(() => {
                realm.create(LESSON_SCHEMA, newLesson);
                resolve(newLesson)
            })
        }).catch(err => {
            reject(err)
        })
    }
    
})

export const updateLesson = ( newLesson ) => new Promise((resolve, reject) => {
    Realm.open( databaseOptions ).then(realm => {
        realm.write(() => {
            realm.create(LESSON_SCHEMA, newLesson);
            resolve(newLesson)
        }).catch(err => {
            reject(err)
        })
    })
})


export const deleteAllLessons = obj => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            realm.delete(realm.objects(obj))
            resolve(null)
        })
    }).catch(err => {
        reject(err)
    })
})