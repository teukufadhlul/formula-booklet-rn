import Realm from 'realm';
import { databaseOptions } from '@data/realm';
import { SUBSECTION_SCHEMA } from '@data/realm/schema/subsection';

export const updateSubsection = newSubsection => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            realm.create(SUBSECTION_SCHEMA, newSubsection);
            resolve(newSubsection)
        })
    }).catch(err => {
        reject(err)
    })
})