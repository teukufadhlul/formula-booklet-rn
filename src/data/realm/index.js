import Schemas from './schema';

export const databaseOptions = {
    schema: Schemas,
    schemaVersion: 14,
    deleteRealmIfMigrationNeeded: true
}

// export default new Realm(databaseOptions)