import LessonSchema from './lesson';
import SectionSchema from './section';
import SubsectionSchema from './subsection';
import HistorySchema from './history';
import FormulaSchema from './formula';
import LessonFilterSchema from './lessonFilter';

const Schemas = [
    LessonSchema, 
    SectionSchema,
    SubsectionSchema,
    HistorySchema,
    FormulaSchema,
    LessonFilterSchema
]

export default Schemas;