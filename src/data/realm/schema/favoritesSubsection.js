export const FAVORITES_SUBSECTION_SCHEMA

const FavoritesSubsectionSchema = {
    name : FAVORITES_SUBSECTION_SCHEMA,
    primaryKey: 'id',
    properties: {
        id: 'int',
        title: { type: 'string', indexed: true },
        subtitle: 'string',
        image: 'string',
        inventor: 'string',
        inventorImg: 'string',
        explanation: 'string',
        formula: 'string'
    }
}