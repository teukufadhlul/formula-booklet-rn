export const SECTION_SCHEMA = 'Section';

const SectionSchema = {
    name: SECTION_SCHEMA,
    primaryKey: 'id',
    properties: {
        id: 'int',
        category: 'string',
        title: 'string',
        subtitle: 'string',
        image: 'string',
        formFactor: 'string',
        subsection: 'Subsection[]'
    }
}

export default SectionSchema;