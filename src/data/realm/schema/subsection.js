export const SUBSECTION_SCHEMA = 'Subsection'

const SubsectionSchema = {
    name: SUBSECTION_SCHEMA,
    primaryKey: 'id',
    properties: {
        id: 'int',
        isFavorite: 'bool',
        title: { type: 'string', indexed: true },
        subtitle: 'string',
        image: 'string',
        inventor: 'string',
        inventorImg: 'string',
        explanation: 'string',
        formula: 'Formula[]'
    }
}

export default SubsectionSchema;