export const HISTORY_SCHEMA = 'History';

const HistorySchema = {
    name: HISTORY_SCHEMA,
    primaryKey: 'id',
    properties: {
        id: 'int',
        history: 'Section[]'
    }
} 

export default HistorySchema;