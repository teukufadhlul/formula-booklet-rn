export const FORMULA_SCHEMA = 'Formula';

const FormulaSchema = {
    name: FORMULA_SCHEMA,
    primaryKey: 'id',
    properties: {
        id: 'int',
        formula: 'string',
        argument: 'string'
    }
}

export default FormulaSchema;