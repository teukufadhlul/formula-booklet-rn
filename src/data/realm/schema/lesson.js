export const LESSON_SCHEMA = 'Lesson'

const LessonSchema = {
    name: LESSON_SCHEMA,
    primaryKey: 'id',
    properties: {
        id: 'int',
        subjects: 'string',
        description: 'string',
        author: 'string',
        image: 'string',
        section: 'Section[]',
        filter: 'Lesson-Filter[]'
    }
}

export default LessonSchema;