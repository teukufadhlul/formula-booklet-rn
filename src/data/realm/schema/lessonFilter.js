export const LESSON_FILTER_SCHEMA = 'Lesson-Filter'

const LessonFilterSchema = {
    name : LESSON_FILTER_SCHEMA,
    primaryKey: 'id',
    properties: {
        id: 'int',
        filter: { type: 'string', indexed: true }
    }
}

export default LessonFilterSchema;