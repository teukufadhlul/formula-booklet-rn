const algebra = require('algebra.js');

const calculation = (_formula, _variables) => {
    class Calculation{
        constructor(formula, variables){
            this.state = {
                formula,
                variables
            }
        }

        decimalToFraction(_decimal){
            let fraction = [1],
                decimal = _decimal.split(',')[1].split('')
        
            decimal.forEach(() => fraction.push(0))
        
            return _decimal.replace(/[,]/g, '') + "/" + fraction.join().replace(/[,]/g,'')
        }

        transformState(){
            const { formula, variables } = this.state,
                  arrayFormula = formula.split(' ')
            let form = [],
                result = {
                    variables : {}
                }
        
            arrayFormula.forEach((item, index) => {
                let variable = variables[item]
                
                if (item.match(/[a-zA-Z]/g)) {
                    result.variables = { 
                        ...result.variables, 
                        [item]: variable == undefined ? null : variable 
                    }

                    if ((`${variable}`).search(',') > 0) {
                        form.push(`(${this.decimalToFraction(variable)})`)
                    } else if (variable || variable == 0) {
                        form.push(`(${variable})`)
                    } else {
                        this.resolve = item
                        form.push(item)
                    }

                } else if (item !== '.') {
                    form.push(item)
                }

            })

            result.problem = form.join().replace(/[,]/g, '')
            
            return result
        }

        result(){
            const { problem, variables } = this.transformState()
            let equation = algebra.parse(problem),
                result = []

            result.push(
                problem.replace(/[]/g, " "), 
                equation.toString(),
                `${this.resolve} = ${equation.solveFor(this.resolve).toString()}`
            )

            return {result, variables}
        }
    }
    
    return new Calculation(_formula, _variables)

}

export default calculation;