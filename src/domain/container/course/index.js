import { compose, withHandlers, lifecycle, withState } from 'recompose';
import calculation from '@domain/calculation'

export const enhanceCourse = compose(
    withHandlers({
        calculate: props => (_formula, _variables) => {
            const x = calculation(_formula, _variables).result()
            return x
        }
    })
)