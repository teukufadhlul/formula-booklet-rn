class Calculation {
    constructor(variables){
        this.state = ({
            x: variables.x,
            y:variables.y
        })
    }

    multiple(){
        return this.state.x * this.state.y
    }

    subtraction(){
        return this.state.x - this.state.y
    }
    
    addition(){
        return this.state.x + this.state.y
    }

    divide(){
        return this.state.x / this.state.y
    }

}

const far = {
    x: 1,
    y: 2
}

const a = new Calculation(far)

const method = 'divide'

// console.tron(a[method]())

import * as React from 'react';
import { Text, View, StyleSheet, TextInput } from 'react-native';
import { Constants } from 'expo';

// You can import from local files

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

export default class App extends React.Component {
  constructor(){
    super()
    this.state = {
      formula: "VO = VT + A * T ^2"
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.paragraph}>
          
        </Text>
        <View style={styles.card} >
          { this.state.formula.split(" ").map((item, index) => {
            if (item.match(/[a-zA-Z]/g)){
              return (
                <TextInput
                   style={styles.texInput}
                   placeholder={item}
                 />
              )
            } else if (item == '*') {
              return <Text> x </Text>
            } else {
              return <Text>{item}</Text>
            }
          })
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  card: {
    flexDirection: 'row'
  },
});
