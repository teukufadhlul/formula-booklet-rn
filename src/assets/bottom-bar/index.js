export { default as imgHistory } from './history-clock-button.png';
export { default as imgMolecule } from './biological-molecule.png';
export { default as imgCollege } from './college-graduation.png'; 
export { default as imgUser } from './user.png';