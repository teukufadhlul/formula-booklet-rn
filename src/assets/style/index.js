export const theme = {
    color: {
        primary: '#00B6DD',
        secondary: '#318FA3',
        shade: '#004D5E',
        bottomBar: 'rgba(0, 77, 94, 0.35)',
        bottomBarActive:'#004D5E',
        light: '#42C2DD',
        danger: '#ED3040'
    },
    fonts: {
        primary: 'Raleway',
        secondary: 'Thasadith'
    }
}