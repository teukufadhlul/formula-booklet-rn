export const images = {
    karol_homepage: require('@assets/img/karol_homepage.jpg'),
    design_party: require('@assets/img/design_party.png'),
    undraw_logistics: require('@assets/img/undraw_logistics.png'),
    kinematika_gerak: require('@assets/img/card-bg-1.jpg'),
    undraw_empty: require('@assets/img/undraw_empty.png'),
    galileo_galilei: require('@assets/img/galileo-galilei.png'),
    undraw_target: require('@assets/img/undraw_target.png'),
    undraw_rocket: require('@assets/img/undraw_rocket.png'),
    urban_dictionary: require('@assets/img/card-bg-2.png')
}